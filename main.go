package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/keystore"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"

	// vrange
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/semver"

	// parser plugins
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/sbt"

	// builder plugins
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/gradle"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/maven"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/sbt"
)

const (
	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
)

var (
	errNoInputFile = errors.New("no supported file")
	errNoBuilder   = errors.New("no builder for requirements file")
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []*cli.Command{
		findCommand(),
		runCommand(),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func findCommand() *cli.Command {
	return &cli.Command{
		Name:      "find",
		Aliases:   []string{"f"},
		Usage:     "Find compatible files in a directory",
		ArgsUsage: "[directory]",
		Flags:     finder.Flags(finder.PresetGemnasiumMaven),
		Action: func(c *cli.Context) error {
			// one argument is expected
			if c.Args().Len() != 1 {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}
			dir := c.Args().Get(0)

			// find compatible files
			finder, err := finder.NewFinder(c, finder.PresetGemnasiumMaven)
			if err != nil {
				return err
			}
			projects, err := finder.FindProjects(dir)
			if err != nil {
				return err
			}
			for _, project := range projects {
				for _, file := range project.Files {
					fmt.Println(project.FilePath(file))
				}
			}

			return nil
		},
	}
}

func runCommand() *cli.Command {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:    flagTargetDir,
			Usage:   "Target directory",
			EnvVars: []string{command.EnvVarTargetDir, command.EnvVarCIProjectDir},
		},
		&cli.StringFlag{
			Name:    flagArtifactDir,
			Usage:   "Artifact directory",
			EnvVars: []string{command.EnvVarArtifactDir, command.EnvVarCIProjectDir},
		},
	}

	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, finder.Flags(finder.PresetGemnasiumMaven)...)
	flags = append(flags, builder.Flags()...)
	flags = append(flags, scanner.Flags()...)
	flags = append(flags, vrange.Flags()...)

	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			startTime := report.ScanTime(time.Now())

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// configure CA certificates
			if err := keystore.Update(c); err != nil {
				return err
			}

			// configure builders
			if err := builder.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// find supported projects
			find, err := finder.NewFinder(c, finder.PresetGemnasiumMaven)
			if err != nil {
				return err
			}
			projects, err := find.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			// build root projects and their sub-projects, if any
			// NOTE: only one root project is expected, but this is a loop for consistency
			// with the main gemnasium project; see https://gitlab.com/gitlab-org/gitlab/-/issues/292952
			subprojects := []finder.Project{}
			for i, p := range projects {
				// calculate path to build file
				reqFile, found := p.RequirementsFile()
				if !found {
					continue
				}
				inputPath := filepath.Join(targetDir, p.FilePath(reqFile))
				log.Debugf("Exporting dependencies for %s", inputPath)

				// find builder
				pkgManager := p.PackageManager.Name
				b := builder.Lookup(pkgManager)
				if b == nil {
					log.Errorf("No builder for package manager %s", pkgManager)
					return errNoBuilder
				}

				// build root project and its sub-projects, if any
				rootExportPath, childExportPaths, err := b.Build(inputPath)
				switch err {
				case nil:
					// root project has dependencies listed in root export
					rootExportFilename := filepath.Base(rootExportPath)
					projects[i].AddScannableFilename(rootExportFilename)
				case builder.ErrNoDependencies:
					// root project has no dependencies
				default:
					// error not handled
					return err
				}

				// collect sub-projects
				//
				// NOTE: A project must have a requirement file so that its dependencies and vulnerabilities
				// can be listed in the generated reports. However, right now it's impossible to track
				// build files of subprojects, and subprojects might not even have build files anyways.
				// To work around this, we create a subproject with a build file
				// whose filename matches the one of the parent build file.
				//
				// TODO: update gemnasium/scanner to accept an alias for finder.File
				// See https://gitlab.com/gitlab-org/gitlab/-/issues/336131
				//
				for _, absExportPath := range childExportPaths {
					log.Debugf("Found dependency export for sub-project in %s", absExportPath)

					// get export path relative to scanned directory
					exportPath, err := filepath.Rel(targetDir, absExportPath)
					if err != nil {
						return err
					}
					subdir, exportFilename := filepath.Split(exportPath)

					// skip subproject if relative export path is excluded
					if find.IsExcludedPath(exportPath) {
						log.Debugf("skip sub-project because export file is excluded: %s", exportPath)
						continue
					}

					// skip subproject if relative build file is excluded
					buildFilePath := filepath.Join(subdir, reqFile.Filename)
					if find.IsExcludedPath(buildFilePath) {
						log.Debugf("skip sub-project because build file is excluded: %s", buildFilePath)
						continue
					}

					// create subproject with scannable graph export and fake build file
					subproject := finder.Project{
						Dir: subdir,
						Files: []finder.File{
							{
								Filename: reqFile.Filename,
								FileType: finder.FileTypeRequirements,
							},
							{
								Filename: exportFilename,
								FileType: finder.FileTypeGraphExport,
							},
						},
						PackageManager: projects[i].PackageManager,
					}

					subprojects = append(subprojects, subproject)
				}
			}

			// scan target directory
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, append(projects, subprojects...))
			if err != nil {
				return err
			}

			// convert to generic report
			var prependPath = "" // empty because the analyzer scans the root directory
			var vulnerabilityReport = convert.ToReport(result, prependPath, &startTime)

			// override scanner information
			// TODO pass metadata.ReportScanner to convert.ToReport instead
			vulnerabilityReport.Scan.Scanner = metadata.ReportScanner
			vulnerabilityReport.Scan.Status = report.StatusSuccess

			vulnerabilityReport.Sort()

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(vulnerabilityReport)
		},
	}
}
