# Gemnasium Maven analyzer changelog

## v2.24.8
- Update build-time certificate directory permission grant order to fix issue using `ADDITIONAL_CA_CERT_BUNDLE` in OpenShift environments  (!156)

## v2.24.7
- Bump `gemnasium-gradle-plugin` to `0.3.5` to print out unresolved gradle dependencies (!153)

## v2.24.6
- Set sbt default to `1.6.1` and `log4j 2.17.1` (!152)

## v2.24.5
- Skip self-edges to prevent `panic: simple: adding self edge [recovered]` errors (!151)

## v2.24.4
- Update to `sbt v1.5.8` and `logj4 2.17.0` (!149)

## v2.24.3
- Update to `sbt v1.5.7` and `logj4 2.16.0` (!146)

## v2.24.2
- Fix a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!144)

## v2.24.1
- Show INFO message when a single directory is scanned, and other directories are skipped (!139)

## v2.24.0
- Use `DS_EXCLUDED_PATHS` variable to filter out paths prior to the scan (!140)

## v2.23.5
- Bump gemnasium-maven-plugin to v0.5.0 (!133)

## v2.23.4
- Don't skip Gradle sub-projects with a custom build file name. (!131)

## v2.23.3
- Set `DS_REPORT_PACKAGE_MANAGER_MAVEN_WHEN_JAVA` to `"true"` by default to maintain backward compatibility with older versions of GitLab. (!130)

## v2.23.2
- Change `package_manager` field of JSON reports from `maven` to `gradle` when scanning Gradle projects. This can be reverted by setting `DS_REPORT_PACKAGE_MANAGER_MAVEN_WHEN_JAVA` to `"true"`. (!128)

## v2.23.1
- Upgrade to Go 1.17 (!129)

## v2.23.0
- Add support for sbt projects aggregating sub-projects (!107)

## v2.22.1
- Fix support for java 16 in gradle (!120)

## v2.22.0
- Add support for java 16 (!111)

## v2.21.5
- Update to Security Report Schema `v14.0.0` (!112)

## v2.21.4
- Fix "X is a directory" error in output log (!114)

## v2.21.3
- Add support for java 15 (!109)

## v2.21.2
- Add support for sbt 1.3+ (!95)

## v2.21.1
- Bump gemnasium-gradle-plugin to get latest version (!106)

## v2.21.0
- Change permissions for Red Hat OpenShift compatibility (!104)

## v2.20.4
- Fix error when issuing git pull on remote-only branch (!103)

## v2.20.3
- Update go-cvss to v0.4.0 (!94)

## v2.20.2
- Fix error occurring when parent Gradle module has no dependencies (!92)

## v2.20.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!91)

## v2.20.0
- Detect supported projects, and scan no more than one Java project per directory (!87)

## v2.19.1
- Add default gradle install so that analyzer can scan projects without a gradle wrapper (!85)

## v2.19.0
- Update Go dependencies `common` and `urfave/cli` to remediate vulnerability GMS-2019-2 (!82)

## v2.18.4
- Update all dependencies in `maven-plugin-builder` (!76)

## v2.18.3
- Fix incorrect update of `gemnasium-gradle-plugin` to `v0.3.2` which fixes a vulnerability (!71)

## v2.18.2
- Update `gemnasium-gradle-plugin` to `v0.3.2` which fixes a vulnerability (!70)

## v2.18.1
- Fix `keytool` x509 certificate import command (!67)

## v2.18.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!66)

## v2.17.2
- Update `gemnasium-gradle-plugin` to `v0.3.1` which fixes an unresolved dependency bug (!64)

## v2.17.1
- Update common to `v2.14.0` which allows git to use CA Certificate bundle (!63)

## v2.17.0
- Add scan object to report (!62)

## v2.16.1
- Fix bug causing failure when run in docker-in-docker mode (!58)

## v2.16.0
- Update logging to be standardized across analyzers (!55)

## v2.15.0
- Add `DS_JAVA_VERSION` var to allow changing java version (!53)

## v2.14.2
- Fix link to advisory (!54)

## v2.14.1
- Fix Gradle Kotlin build scripts not being detected (!40)

## v2.14.0
- Add `ADDITIONAL_CA_CERT_BUNDLE` to java keystore (!50)

## v2.13.0
- Add `GRADLE_CLI_OPTS` environment variable (!46)
- Add `SBT_CLI_OPTS` environment variable (!46)

## v2.12.0
- Update gemnasium to `v2.10.0` which adds support for vulnerability Severity levels

## v2.11.0
- Add `id` field to vulnerabilities in JSON report (!44)

## v2.10.0
- Make `gemnasium-maven-plugin` a build-time install in the analyzer image (!42)
- Make `gemnasium-gradle-plugin` a build-time install in the analyzer image (!43)
- Make `sbt-dependency-graph` plugin a build-time install in the analyzer image (!45)

## v2.9.1
- Specify maven and java version for base docker image (!36)

## v2.9.0
- Add support for custom CA certs (!33)

## v2.8.0
- Bump gemnasium-maven-plugin to v0.4.0 (!35)

## v2.7.0
- Use `MAVEN_CLI_OPTS` with all invocations of maven (@fcbrooks) (!21)

## v2.6.0
- Add support for projects built with sbt (!20)

## v2.5.0
- Add support for projects built with gradle (!17)

## v2.4.0
- Add `MAVEN_CLI_OPTS` variable with `-DskipTests --batch-mode` default (!16)

## v2.3.0
- Use gemnasium-db git repo instead of the Gemnasium API (!15)

## v2.2.4
- Run `mvn install` before analysis to fix multi-modules support when using internal dependencies between modules

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!13)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!11)

## v2.2.1
- Sort the dependency files and their dependencies (!9)

## v2.2.0
- List the dependency files and their dependencies (!8)

## v2.1.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.1.1
- Fix multi-modules support

## v2.1.0
- Bump common to v2.1.4, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
