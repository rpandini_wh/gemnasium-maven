// Package keystore provides high-level functions to update the Java keystore.
// It uses the cacert package of the analyzers common library to set the CA bundle.
package keystore

import (
	"os"
	"os/exec"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
)

// Update updates the CA cert bundle in the Java keystore, if defined
func Update(c *cli.Context) error {
	if !c.IsSet(cacert.FlagBundleContent) {
		return nil
	}
	cmd := importCmd(os.Getenv("JAVA_HOME"))
	cmd.Dir = "/"
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

func importCmd(javaHome string) *exec.Cmd {
	return exec.Command(
		"keytool",
		"-importcert",
		"-alias", "custom",
		"-file", cacert.DefaultBundlePath,
		"-trustcacerts",
		"-noprompt",
		"-storepass", "changeit",
		"-keystore", keystorePathFor(javaHome),
	)
}

func keystorePathFor(javaHome string) string {
	if isJava8(javaHome) {
		return path.Join(javaHome, "jre/lib/security/cacerts")
	}
	return path.Join(javaHome, "lib/security/cacerts")
}

func isJava8(javaHome string) bool {
	_, file := path.Split(javaHome)
	return strings.HasPrefix(file, "adoptopenjdk-8")
}
