package exportpath

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
)

func TestExtract(t *testing.T) {
	tcs := []struct {
		name    string
		input   string
		extract func(output []byte) ([]string, error)
		want    []string
	}{

		// Gradle
		{
			"ExtractGradle",
			`
Welcome to Gradle 5.6.4!
Here are the highlights of this release:
- Incremental Groovy compilation
- Groovy compile avoidance
- Test fixtures for Java projects
- Manage plugin versions via settings script
For more details see https://docs.gradle.org/5.6.4/release-notes.html
Starting a Gradle Daemon (subsequent builds will be faster)
> Task :gemnasiumDumpDependencies
Writing dependency JSON to /builds/gitlab-org/security-products/tests/java-gradle-multimodules/gradle-dependencies.json
> Task :api:gemnasiumDumpDependencies
Writing dependency JSON to /builds/gitlab-org/security-products/tests/java-gradle-multimodules/api/gradle-dependencies.json
> Task :model:gemnasiumDumpDependencies
Writing dependency JSON to /builds/gitlab-org/security-products/tests/java-gradle-multimodules/model/gradle-dependencies.json
> Task :web:gemnasiumDumpDependencies
Writing dependency JSON to /builds/gitlab-org/security-products/tests/java-gradle-multimodules/web/gradle-dependencies.json
BUILD SUCCESSFUL in 21s
4 actionable tasks: 4 executed`,
			ExtractGradle,
			[]string{
				"/builds/gitlab-org/security-products/tests/java-gradle-multimodules/gradle-dependencies.json",
				"/builds/gitlab-org/security-products/tests/java-gradle-multimodules/api/gradle-dependencies.json",
				"/builds/gitlab-org/security-products/tests/java-gradle-multimodules/model/gradle-dependencies.json",
				"/builds/gitlab-org/security-products/tests/java-gradle-multimodules/web/gradle-dependencies.json",
			},
		},

		// Maven
		{
			"ExtractMaven",
			`
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO]
[INFO] java-maven-multi-modules                                           [pom]
[INFO] model                                                              [jar]
[INFO] api                                                                [jar]
[INFO] web                                                                [jar]
[INFO]
[INFO] ----< com.gitlab.security_products.tests:java-maven-multi-modules >-----
[INFO] Building java-maven-multi-modules 1.0-SNAPSHOT                     [1/4]
[INFO] --------------------------------[ pom ]---------------------------------
[INFO]
[INFO] --- gemnasium-maven-plugin:0.4.0:dump-dependencies (default-cli) @ java-maven-multi-modules ---
[INFO] Gemnasium Maven Plugin
[INFO]
[INFO] Project's dependencies have been succesfully dumped into: /builds/gitlab-org/security-products/tests/java-maven-multimodules/gemnasium-maven-plugin.json
[INFO]
[INFO] --------------< com.gitlab.security_products.tests:model >--------------
[INFO] Building model 1.0-SNAPSHOT                                        [2/4]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- gemnasium-maven-plugin:0.4.0:dump-dependencies (default-cli) @ model ---
[INFO] Gemnasium Maven Plugin
[INFO]
[INFO] Project's dependencies have been succesfully dumped into: /builds/gitlab-org/security-products/tests/java-maven-multimodules/model/gemnasium-maven-plugin.json
[INFO]
[INFO] ---------------< com.gitlab.security_products.tests:api >---------------
[INFO] Building api 1.0-SNAPSHOT                                          [3/4]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- gemnasium-maven-plugin:0.4.0:dump-dependencies (default-cli) @ api ---
[INFO] Gemnasium Maven Plugin
[INFO]
[INFO] Project's dependencies have been succesfully dumped into: /builds/gitlab-org/security-products/tests/java-maven-multimodules/api/gemnasium-maven-plugin.json
[INFO]
[INFO] ---------------< com.gitlab.security_products.tests:web >---------------
[INFO] Building web 1.0-SNAPSHOT                                          [4/4]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- gemnasium-maven-plugin:0.4.0:dump-dependencies (default-cli) @ web ---
[INFO] Gemnasium Maven Plugin
[INFO]
[INFO] Project's dependencies have been succesfully dumped into: /builds/gitlab-org/security-products/tests/java-maven-multimodules/web/gemnasium-maven-plugin.json
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary for java-maven-multi-modules 1.0-SNAPSHOT:
[INFO]
[INFO] java-maven-multi-modules ........................... SUCCESS [  1.332 s]
[INFO] model .............................................. SUCCESS [  0.077 s]
[INFO] api ................................................ SUCCESS [  0.198 s]
[INFO] web ................................................ SUCCESS [  0.065 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.966 s
[INFO] Finished at: 2021-11-03T20:05:36Z
[INFO] ------------------------------------------------------------------------`,
			ExtractMaven,
			[]string{
				"/builds/gitlab-org/security-products/tests/java-maven-multimodules/gemnasium-maven-plugin.json",
				"/builds/gitlab-org/security-products/tests/java-maven-multimodules/model/gemnasium-maven-plugin.json",
				"/builds/gitlab-org/security-products/tests/java-maven-multimodules/api/gemnasium-maven-plugin.json",
				"/builds/gitlab-org/security-products/tests/java-maven-multimodules/web/gemnasium-maven-plugin.json",
			},
		},

		// Sbt
		{
			"ExtractSbt",
			`
[info] Loading settings from plugins.sbt ...
[info] Loading global plugins from /root/.sbt/1.0/plugins
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by sbt.internal.librarymanagement.ivyint.ErrorMessageAuthenticator$ (file:/root/.sbt/boot/scala-2.12.3/org.scala-sbt/sbt/1.0.2/librarymanagement-ivy_2.12-1.0.2.jar) to field java.net.Authenticator.theAuthenticator
WARNING: Please consider reporting this to the maintainers of sbt.internal.librarymanagement.ivyint.ErrorMessageAuthenticator$
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
[info] Loading settings from plugins.sbt ...
[info] Loading project definition from /tmp/project/project
[info] Loading settings from build.sbt ...
[info] Set current project to sbt-multi-project-example (in build file:/tmp/project/)
[success] Total time: 1 s, completed Feb 25, 2021, 8:17:28 PM
[info] Wrote dependency graph to '/tmp/project/target/dependencies-compile.dot'
[info] Wrote dependency graph to '/tmp/project/common/target/dependencies-compile.dot'
[info] Updating {file:/tmp/project/}multi1...
[info] Updating {file:/tmp/project/}multi2...
[info] Done updating.
[info] Wrote dependency graph to '/tmp/project/multi2/target/dependencies-compile.dot'
[info] Done updating.
[info] Wrote dependency graph to '/tmp/project/multi1/target/dependencies-compile.dot'
[success] Total time: 6 s, completed Feb 25, 2021, 8:17:34 PM`,
			ExtractSbt,
			[]string{
				"/tmp/project/target/dependencies-compile.dot",
				"/tmp/project/common/target/dependencies-compile.dot",
				"/tmp/project/multi2/target/dependencies-compile.dot",
				"/tmp/project/multi1/target/dependencies-compile.dot",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.extract([]byte(tc.input))
			require.NoError(t, err)
			require.ElementsMatch(t, tc.want, got)
		})
	}
}

func TestSplit(t *testing.T) {
	t.Run("with root path", func(t *testing.T) {
		wantSubpaths := []string{
			"/project/sub1/child-export.json",
			"/project/sub2/child-export.json",
			"/projectsub2/sub21/grand-child-export.json",
			"/projectsub2/sub22/grand-child-export.json",
		}
		wantRootpath := "/project/root-export.json"

		// create input by inserting root path in the middle of sub paths
		paths := make([]string, len(wantSubpaths))
		copy(paths, wantSubpaths)
		paths = append(paths[:2], append([]string{wantRootpath}, paths[2:]...)...)

		rootpath, subpaths, err := Split(paths, "/project")
		require.NoError(t, err)
		require.Equal(t, wantRootpath, rootpath)
		require.ElementsMatch(t, wantSubpaths, subpaths)
	})

	t.Run("without root path", func(t *testing.T) {
		wantSubpaths := []string{
			"/project/sub1/child-export.json",
			"/project/sub2/child-export.json",
		}

		_, subpaths, err := Split(wantSubpaths, "/project")
		require.ErrorIs(t, err, builder.ErrNoDependencies)
		require.ElementsMatch(t, wantSubpaths, subpaths)
	})

}
