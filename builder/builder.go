package builder

import (
	"errors"

	"github.com/urfave/cli/v2"
)

// ErrNoDependencies is returned when the builder finds no dependencies
var ErrNoDependencies = errors.New("No dependencies")

// Builder is implemented by what can provide a generate a lock file or dependency graph for project.
type Builder interface {
	// Build generates an output file for the given input file, and returns its name along with the paths
	// of outputs for any sub-projects discovered during the Build step
	Build(string) (string, []string, error)
}

// Configurable is implemented by builders that can be configured using CLI flags
type Configurable interface {
	Flags() []cli.Flag            // Flags returns the CLI flags that configure the builder
	Configure(*cli.Context) error // Configure configures the builder based on a CLI context
}
