package builder

import (
	"sync"

	"github.com/urfave/cli/v2"
)

var buildersMu sync.Mutex
var builders = make(map[string]Builder)

// Register registers a builder with a package manager name.
func Register(pkgManager string, builder Builder) {
	buildersMu.Lock()
	defer buildersMu.Unlock()
	if _, dup := builders[pkgManager]; dup {
		panic("Register called twice for package manager name " + pkgManager)
	}
	builders[pkgManager] = builder
}

// Lookup looks for a builder that matches a package manager name.
func Lookup(pkgManager string) Builder {
	buildersMu.Lock()
	defer buildersMu.Unlock()
	for name, b := range builders {
		if name == pkgManager {
			return b
		}
	}
	return nil
}

// Flags returns the flags of all configurable builders
func Flags() []cli.Flag {
	buildersMu.Lock()
	defer buildersMu.Unlock()
	flags := []cli.Flag{}
	for _, b := range builders {
		if cb, ok := b.(Configurable); ok {
			flags = append(flags, cb.Flags()...)
		}
	}
	return flags
}

// Configure configures all configurable builders
func Configure(c *cli.Context) error {
	buildersMu.Lock()
	defer buildersMu.Unlock()
	for _, b := range builders {
		if cb, ok := b.(Configurable); ok {
			if err := cb.Configure(c); err != nil {
				return err
			}
		}
	}
	return nil
}
