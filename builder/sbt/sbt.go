package sbt

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/exportpath"
)

const flagSbtOpts = "sbt-opts"

// Builder generates dependency lists for sbt projects
type Builder struct {
	SbtOpts string // SbtOpts are the CLI options passed to sbt
}

// Flags returns the CLI flags that configure the sbt command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagSbtOpts,
			Usage:   "Optional CLI arguments for sbt",
			Value:   "",
			EnvVars: []string{"SBT_CLI_OPTS"},
		},
	}
}

// Configure configures the sbt command
func (b *Builder) Configure(c *cli.Context) error {
	b.SbtOpts = c.String(flagSbtOpts)
	return nil
}

// Build generates graph exports for all projects of a multi-project Sbt build.
// It returns the export path for the root project, and the export paths for its sub-projects, if any.
func (b *Builder) Build(input string) (string, []string, error) {
	paths, err := b.generateDotReports(input)
	if err != nil {
		return "", []string{}, err
	}

	// Move DOT exports to the base directory of the corresponding projects.
	// This applies to the root the project and any sub-project.
	// By default, the dependencyDot creates exports to "target/dependencies-<config>.dot".
	//
	// This is currently needed because the files scanned by Gemnasium can't be in a subdirectory.
	// See https://gitlab.com/gitlab-org/gitlab/-/issues/336129.
	//
	paths, err = moveToParentDir(paths)
	if err != nil {
		return "", []string{}, err
	}

	return exportpath.Split(paths, filepath.Dir(input))
}

// generateDotReports writes dependency graphs to DOT files using the "dependencyDot" sbt task.
// It returns the paths to the DOT exports created for the root project and its sub-projects, if any.
func (b Builder) generateDotReports(input string) ([]string, error) {
	// Colors must be disabled otherwise exportpath.ExtractSbt
	// can't trim the quotes that surround the path of the DOT export.
	args := append(strings.Fields(b.SbtOpts), "-no-colors", "dependencyDot")
	cmd := exec.Command("sbt", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return exportpath.ExtractSbt(output)
}

// moveToParentDir moves the given files to their parent directory.
func moveToParentDir(paths []string) ([]string, error) {
	result := []string{}
	for _, path := range paths {
		dir, base := filepath.Split(path)
		dest := filepath.Join(dir, "..", base)
		err := os.Rename(path, dest)
		if err != nil {
			return nil, err
		}
		result = append(result, dest)
	}
	return result, nil
}

func init() {
	builder.Register("sbt", &Builder{})
}
